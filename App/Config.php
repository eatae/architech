<?php
/**
 * Created by PhpStorm.
 * User: Toha
 * Date: 16.10.2016
 * Time: 17:35
 */

namespace App;


class Config
{
    private static $_instance = null;
    public $data;

    private function __construct()
    {
        $config = __DIR__ . '/../config.php';
        if (is_readable($config))
            $this->data = include $config;
    }

    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }
}