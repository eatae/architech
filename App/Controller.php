<?php

namespace App;


abstract class Controller
{
    protected $view;
    protected $access = true;

    public function __construct()
    {
        $this->view = new View();
    }

    public function action($actionName)
    {
        if (!$this->access()) {
            echo $this->view->display(__DIR__ . '/../Templates/do_html_access.php');
        } else {
            method_exists($this, $actionName) ? $this->$actionName() : $this->actionDefault();
        }
    }

    protected function access()
    {
        return $this->access;
    }

    abstract public function actionDefault();
}