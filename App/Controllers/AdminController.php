<?php

namespace App\Controllers;

use App\Models,
    App;

class AdminController extends App\Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->access = $_GET['admin'];
        $this->view->msgHead = 'Панель администратора';
        $this->view->msgAction = '';
        $this->article = new App\Models\Article();
    }

    public function actionOne()
    {
        // $this->view from parent
        $this->view->article = Models\Article::findById($_GET['id']);
        $html = $this->view->display(__DIR__ . '/../../Templates/do_html_article_admin.php');
        echo $html;
    }

    public function actionDelete()
    {
        if (empty($_GET['id'])) {
            echo $this->view->display(__DIR__ . '/../../Templates/do_html_admin_err.php');
            return;
        }

        $this->article->id = $_GET['id'];
        $this->article->delete();

        $this->view->$msgAction = 'Успешно удалили новость';
        $this->actionDefault();
    }

    public function actionDefault()
    {
        // $this->view from paren::Controller
        $this->view->articles = Models\Article::findAll();
        // заголовок страницы
        $html = $this->view->display(__DIR__ . '/../../Templates/do_html_admin_allnews.php');
        echo $html;
    }

    public function actionUpdate()
    {
        if (empty($_GET['id']) && empty($_GET['title']) && empty($_GET['content'])) {
            echo $this->view->display(__DIR__ . '/../../Templates/do_html_admin_err.php');
            return;
        }

        $this->article->id = $_GET['id'];
        $this->article->title = $_GET['title'];
        $this->article->content = $_GET['content'];

        if (!empty($_GET['author_id'])) {
            $this->article->author_id = $_GET['author_id'];
            $this->article->save();
        } else {
            // если не пришёл author_id то оставляем старого (не ставим в подстановки).
            $this->article->save(['author_id']);
        }

        $this->view->$msgAction = 'Успешно обновили новость';
        $this->actionDefault();
    }


    public function actionSave()
    {
        if (empty($_GET['author_id']) && empty($_GET['title']) && empty($_GET['content'])) {
            echo $this->view->display(__DIR__ . '/../../Templates/do_html_admin_err.php');
            return;
        }

        $this->article->title = $_GET['title'];
        $this->article->content = $_GET['content'];
        $this->article->author_id = $_GET['author_id'];
        $this->article->save();

        $this->view->$msgAction = 'Успешно добавили новость';
        $this->actionDefault();
    }


}