<?php

namespace App\Controllers;

require_once __DIR__ . '/../../autoload.php';

use App;
use App\Models;


class IndexController extends App\Controller
{

    public function actionDefault()
    {
        // $this->view from paren::Controller
        $this->view->articles = Models\Article::findAll(5);
        // заголовок страницы
        $this->view->msg = 'Анонс последних новостей';
        // подключаем Templates
        $html = $this->view->display(__DIR__ . '/../../Templates/do_html_index_allnews.php');
        echo $html;
    }


    public function actionOne()
    {
        // $this->view from parent
        $this->view->article = Models\Article::findById($_GET['number']);
        $html = $this->view->display(__DIR__ . '/../../Templates/do_html_article.php');
        echo $html;
    }
}