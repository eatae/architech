<?php

namespace App;

require_once __DIR__ . '/../autoload.php';


class Db
{
    protected $db_hdr;


    public function __construct()
    {
        $this->db_hdr = new \PDO('mysql:host=127.0.0.1; dbname=profit_2', 'root', '');
    }


    public function lastInsertId()
    {
        return $this->db_hdr->lastInsertId();
    }


    public function execute(string $sql, array $data = [])
    {
        $sth = $this->db_hdr->prepare($sql);

        if (!$sth->execute($data)) {
            var_dump($sth->errorInfo());
            //return false;
        }

        return true;
    }


    public function query(string $sql, array $data = [], string $class = null)
    {
        $sth = $this->db_hdr->prepare($sql);

        if (!$sth->execute($data)) {
            var_dump($sth->errorInfo());
            //return false;
        }

        // return Objects
        if (null != $class) {
            return $sth->fetchAll(\PDO::FETCH_CLASS, $class);
        }

        // return Array
        return $sth->fetchAll(\PDO::FETCH_ASSOC);
    }
}