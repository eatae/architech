<?php
namespace App;

require_once __DIR__ . '/../autoload.php';

abstract class Model
{
    public $id;

    public static function findAll($limit = null, $sort = 'DESC')
    {
        $db = new Db();
        $class = static::$class ?: null;
        if (null != $limit) {
            $limit = ' LIMIT ' . $limit;
        }
        $sql = 'SELECT * FROM ' . static::$table . ' ORDER BY id ' . $sort . $limit;

        return $db->query($sql, [], $class);
    }

    public function save(array $skip = null)
    {
        // если id не присвоен или нет такого в базе данных
        if ($this->isNew() or !self::findById($this->id)) {
            $this->insert();
        } else {
            $this->update($skip);
        }
    }

    public function isNew()
    {
        return empty($this->id);
    }

    public static function findById($id)
    {
        $db = new Db();
        $class = static::$class ?: null;

        $sql = 'SELECT * FROM ' . static::$table .
            ' WHERE id = :id';

        // empty array == bool false
        if (!$result = $db->query($sql, [':id' => $id], $class)) {
            return false;
        }

        // return Object
        return $result[0];

    }

    public function insert()
    {
        $db = new Db();

        $columns = [];
        $binds = [];
        $data = [];

        foreach ($this as $col => $val) {
            if ('id' == $col)
                continue;
            $columns[] = $col;
            $binds[] = ':' . $col;
            $data[':' . $col] = $val;
        }

        $sql = 'INSERT INTO ' . static::$table . ' (' . implode(', ', $columns) . ') ' .
            'VALUES(' . implode(', ', $binds) . ')';

        $db->execute($sql, $data);

        $this->id = $db->lastInsertId();

    }

    /**
     * @param array|null $skip
     * $skip - массив, где значения - это свойства объекта, которые нужно пропустить.
     */

    public function update($skip = null)
    {
        $db = new Db();

        $binds = [];
        $data = [':id' => $this->id];

        // если есть поля, которые нужно пропустить
        if (null != $skip) {

            foreach ($this as $col => $val) {

                // пропускаем поля
                foreach ($skip as $skipVal) {
                    if ($col == $skipVal) {
                        continue 2;
                    }
                }
                // в $binds id не нужен, его подставляем в WHERE
                if ('id' == $col) {
                    continue;
                }
                // записываем подстановки и данные для них
                $binds[] = $col . '=:' . $col;
                $data[':' . $col] = $val;
            }

        } // нет полей для пропуска
        else {
            foreach ($this as $col => $val) {
                // в $binds id не нужен, его подставляем в WHERE
                if ('id' == $col) {
                    continue;
                }

                $binds[] = $col . '=:' . $col;
                $data[':' . $col] = $val;

            }
        }

        $sql = 'UPDATE ' . static::$table .
            ' SET ' . implode(', ', $binds) .
            ' WHERE id=:id';

        //echo ($sql);
        $db->execute($sql, $data);
    }

    public function delete()
    {
        if (!$this->isNew()) {
            $db = new Db();
            $sql = 'DELETE FROM ' . static::$table . ' WHERE id=:id';

            $db->execute($sql, [':id' => $this->id]);
        }
    }
}