<?php
namespace App\Models;

use App;

class Article extends App\Model
{
    // $id in Model
    protected static $table = 'articles';
    protected static $class = self::class;
    public $title;
    public $content;
    public $author_id;

    /**
     * @var object $author
     */
    public function __get($key)
    {
        if ('author' == $key && !empty($this->author_id)) {
            return Author::findById($this->author_id);
        }
    }


    public function __isset($key)
    {
        if ('author' == $key && !empty($this->author_id)) {
            return true;
        }
    }

}