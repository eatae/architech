<?php
namespace App\Models;

use App;

class Author extends App\Model
{
    // $id in Model
    protected static $table = 'authors';
    protected static $class = self::class;
    public $firstname;
    public $lastname;

    public function __toString()
    {
        return ($this->firstname . ' ' . $this->lastname);
    }
}