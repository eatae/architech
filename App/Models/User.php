<?php
namespace App\Models;

require_once __DIR__ . '/../../autoload.php';

use App;

class User extends App\Model
{
    // $id in Model
    protected static $table = 'users';
    protected static $class = self::class;
    public $email;
    public $login;
    public $name;
    public $age;
    public $admin;
    private $password;

}