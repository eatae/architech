<?php

namespace App;
/**
 * Class View
 * @package App
 *
 * @property array $articles
 * @property object $article
 * @property string $msgHead
 * @property string $msgAction
 */

class View
{

    use TMagic;

    public function display($template)
    {
        return $this->render($template);
    }


    public function render($template)
    {
        ob_start();
        include $template;
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
}