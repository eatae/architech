<html>
<head>
    <meta charset="utf-8">
    <title>Index</title>
    <style type="text/css">
        table {
            margin: 20px;
            border: 2px solid silver;
        }

        td, th {
            padding: 10px;
            border: 2px solid silver;
        }

        button {
            margin: 22px;
        }
    </style>
</head>
<body>
<h1><?php echo $this->msgHead; ?></h1>

<p><?php echo $this->msgAction; ?></p>
<a href='<?php echo '/index' ?>'> На главную </a>
<table>
    <th>Номер новости</th>
    <th>Название</th>
    <th>Автор</th>

    <?php
    foreach ($this->articles as $val): ?>
        <tr>
            <td><a href='<?php echo '/admin/one/?admin=1&id=' . $val->id; ?>'>Новость № <?php echo $val->id; ?></a></td>
            <td> <?php echo $val->title; ?> </td>
            <td> <?php echo $val->author; ?> </td>
        </tr>
    <?php endforeach ?>
</table>


<!-- Добавление новости -->
<form action='/admin/save/'>
    <input type="hidden" name="admin" value="1">
    <input type="hidden" name="id" value='<?php echo $this->article->id; ?>'>

    <p><b>Новая новость</b></p>

    <p>Новый заголовок: <input type='text' name='title'></p>

    <p>Новое содержание: <input type='text' name='content' size='40'></p>

    <p>Автор: <input type='text' name='author_id' size='40'></p>
    <input type='submit' value='Отправить'>
</form>

</body>
</html>