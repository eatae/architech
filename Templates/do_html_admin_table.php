<?php
switch ($_GET['item']) {
    case 'insert':

        if ($_GET['title'] and $_GET['content'] and $_GET['author_id']) {

            $this->article->title = $_GET['title'];
            $this->article->content = $_GET['content'];
            $this->article->author_id = $_GET['content'];
            $this->article->save();
            $message = 'Успешно добавили новость';

        } else {

            header('Location:' . $_SERVER['PHP_SELF'] . '?admin_err=1');

        };
        break;

    case 'update':

        if ($_GET['title'] and $_GET['content'] and $_GET['id']) {

            $this->article->id = $_GET['id'];
            $this->article->title = $_GET['title'];
            $this->article->content = $_GET['content'];
            // если пришёл author_id не пустым то записываем его(обновляем автора)
            if (!empty($_GET['author_id'])) {
                $this->article->author_id = $_GET['author_id'];
                $this->article->save();
            } else {
                // если не пришёл author_id то оставляем старого (не ставим в подстановки).
                $this->article->save(['author_id']);
            }

            $message = 'Успешно обновили новость';

        } else {

            header('Location:' . $_SERVER['PHP_SELF'] . '?admin_err=1');

        };
        break;

    case 'delete':

        if ($_GET['id']) {

            $this->article->id = $_GET['id'];
            $this->article->delete();
            $message = 'Успешно удалили новость';

        } else {

            header('Location:' . $_SERVER['PHP_SELF'] . '?admin_err=1');

        };
        break;

    default:
        $message = '';
}
?>


<html>
<head>
    <meta charset="utf-8">
    <title>Admin</title>
    <style type="text/css">
        form {
            border: 2px silver solid;
            padding: 12px;

        }
    </style>
</head>
<body>
<a href='<?php echo $_SERVER['PHP_SELF'] ?>'> Главная </a>

<h2><?php echo $message ?></h2>


<!-- Добавление новости -->
<form action='<?php echo $_SERVER['PHP_SELF'] ?>'>
    <!--  hidden admin for index  -->
    <input type="hidden" name="admin" value="1">
    <input type="hidden" name="item" value="insert">

    <p><b>Новая новость:</b></p>

    <p>Заголовок: <input type='text' name='title'></p>

    <p>Содержание <input type='text' name='content' size='40'></p>

    <p>Автор <input type='text' name='author_id' size='40'></p>
    <input type='submit' value='Отправить'>
</form>


<!-- Изменение новости -->
<form action='<?php echo $_SERVER['PHP_SELF'] ?>'>
    <!--  hidden admin for index  -->
    <input type="hidden" name="admin" value="1">
    <input type="hidden" name="item" value="update">

    <p><b>Обновление новости</b></p>

    <p>Номер новости (id): <input type='text' name='id'></p>

    <p>Новый заголовок: <input type='text' name='title'></p>

    <p>Новое содержание <input type='text' name='content' size='40'></p>

    <p>необязательное поле (Автор) <input type='text' name='author_id' size='40'></p>
    <input type='submit' value='Отправить'>
</form>


<!-- Удаление новости -->
<form action='<?php echo $_SERVER['PHP_SELF'] ?>'>
    <!--  hidden admin for index  -->
    <input type="hidden" name="admin" value="1">
    <input type="hidden" name="item" value="delete">

    <p><b>Удаление новости</b></p>

    <p>Номер новости (id): <input type='text' name='id'></p>
    <input type='submit' value='Отправить'>
</form>
</body>
