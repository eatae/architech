<html>
<head>
    <meta charset="utf-8">
    <title>Index</title>
    <style type="text/css">
        h1 {
            color: darkslategrey;
            font: 600 15pt serif;
        }

        h2 {
            margin: 20px;
            color: darkslateblue;
            font: 600 16pt serif;
        }

        div {
            margin: 20px;
            color: darkslategrey;
            font: 15pt serif;
        }
    </style>
</head>
<body>


<?php
if (!is_object($this->article)) {
    echo 'Такой записи не существует';
    echo "<p><a href='{$_SERVER['PHP_SELF']}'>На главную</a></p>";
    exit;
}

echo "<h1>Вы читаете новость № {$this->article->id}</h1>\n
          <h2>Это - {$this->article->title}</h2>\n
          <div>Сама статья:<br>{$this->article->content}</div>\n
          <div>Автор:<br>{$this->article->author}</div>\n
          <a href='{$_SERVER['PHP_SELF']}'>На главную</a>";
?>

</body>
</html>