<html>
<head>
    <meta charset="utf-8">
    <title>Index</title>
    <style type="text/css">
        h1 {
            color: darkslategrey;
            font: 600 15pt serif;
        }

        h2 {
            margin: 20px;
            color: darkslateblue;
            font: 600 16pt serif;
        }

        div {
            margin: 20px;
            color: darkslategrey;
            font: 15pt serif;
        }

        .articleAdmin {
            border: 1px solid grey;
            margin: 5px;
            padding: 20px;
            color: darkslategrey;
            font: 12pt serif;
        }
    </style>
</head>
<body>


<?php
if (!is_object($this->article)) {
    echo 'Такой записи не существует';
    echo "<p><a href='/admin'>назад в админ-панель</a></p>";
    exit;
}

echo "<h1>Вы читаете новость № {$this->article->id}</h1>\n
          <h2>{$this->article->title}</h2>\n
          <div>Сама статья:<br>{$this->article->content}</div>\n
          <div>Автор:<br>{$this->article->author}</div>\n
          <p><a href='/admin/?admin=1'>назад в админ-панель</a></p>";
?>


<!-- Изменение новости -->
<div class="articleAdmin">
    <form action='/admin/update/'>
        <input type="hidden" name="admin" value="1">
        <input type="hidden" name="id" value='<?php echo $this->article->id; ?>'>

        <p><b>Изменить новость</b></p>

        <p>Новый заголовок: <input type='text' name='title'></p>

        <p>Новое содержание <input type='text' name='content' size='40'></p>

        <p>необязательное поле (Автор) <input type='text' name='author_id' size='40'></p>
        <input type='submit' value='Отправить'>
    </form>
</div>


<!-- Удаление новости -->
<div class="articleAdmin">
    <form action='/admin/delete/'>
        <input type="hidden" name="admin" value="1">
        <input type="hidden" name="id" value='<?php echo $this->article->id; ?>'>

        <p><b>Удалить новость</b></p>
        <input type='submit' value='Отправить'>
    </form>
</div>
</body>
</html>