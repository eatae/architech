<html>
<head>
    <meta charset="utf-8">
    <title>Index</title>
    <style type="text/css">
        table {
            margin: 20px;
            border: 2px solid silver;
        }

        td, th {
            padding: 10px;
            border: 2px solid silver;
        }

        button {
            margin: 22px;
        }
    </style>
</head>
<body>
<h1><?php echo $this->msg; ?></h1>
<table>
    <th>Ссылка на новость</th>
    <th>Название</th>
    <th>Автор</th>

    <?php
    foreach ($this->articles as $val): ?>
        <tr>
            <td><a href='<?php echo 'article/one/?number=' . $val->id; ?>'>Новость № <?php echo $val->id; ?></a></td>
            <td> <?php echo $val->title; ?> </td>
            <td> <?php echo $val->author; ?> </td>
        </tr>
    <?php endforeach ?>
</table>

<!--  Вход в админку с доступом  -->

<!--  Доступ закрыт  -->
<p>Доступ закрыт:</p>
<a href='<?php echo '/admin/default' ?>'> Админка err</a>

<p>Доступ открыт:</p>
<a href='<?php echo 'admin/?admin=1' ?>'> Админка </a>
</body>
</html>