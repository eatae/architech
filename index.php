<?php

require_once __DIR__ . '/autoload.php';

// разбиваем запрос
$parts = explode('/', $_SERVER['REQUEST_URI']);

if ($parts[1] == 'index.php') {
    $parts[1] = 'index';
}

// print_r($parts);

// получаем имя контроллера и проверяем есть ли
$request = (!empty($parts[1]) && is_readable(__DIR__ . '/App/Controllers/' . $parts[1] . 'Controller.php')) ?
    $parts[1] . 'Controller' :
    'IndexController';
$className = '\App\Controllers\\' . ucfirst($request);
$ctrl = new $className();


$action = !empty($parts[2]) ? $parts[2] : 'Default';
$actionName = 'action' . ucfirst($action);

$ctrl->action($actionName);















/*
=======================================
    OLD INDEX
=======================================

$view = new \App\View();

// смотрим одну новость
if ( $_GET['number'] ) {
    $view->article = Models\Article::findById($_GET['number']);
    $html = $view->display(__DIR__ . '/Templates/do_html_article.php');
}
// админка
elseif( $_GET['admin'] ) {
    $view->article = new Models\Article();
    $html = $view->display(__DIR__ . '/Templates/do_html_admin_table.php');
}
// админка, не пришли данные
elseif( $_GET['admin_err'] ) {
    $html = $view->display(__DIR__ . '/Templates/do_html_admin_form_err.php');
    }
// index
else {
    $view->articles = Models\Article::findAll();
    // получаем данные из буфера
    $html = $view->render(__DIR__ . '/Templates/do_html_index_allnews.php');
    // можно обработать эти данные
    $html = str_replace('Москве', 'МОСКВЕ', $html);
}

// показываем страницу
echo $html;
*/


