CREATE DATABASE profit_2;

USE profit_2;

CREATE TABLE users (
	id MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	email VARCHAR(30),
	login VARCHAR(30) NOT NULL,
	name VARCHAR(30),
	age SMALLINT(3) UNSIGNED
)ENGINE=innoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;



INSERT INTO users (email, login, name, age) 
	VALUES ('john@mail.com', 'john', 'John', '25'),
			('mike@mail.com', 'miki', 'Mike', '31'),
			('alice@mail.com', 'alice', 'Alice', '26'),
			('bob@mail.com', 'bob', 'Bob', '33'),
			('eva@mail.com', 'eva', 'Eva', '32');



===========================================================		


		
CREATE TABLE articles (
	id MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	title VARCHAR(255),
	content TEXT
)ENGINE=innoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


INSERT INTO articles (title, content) 
	VALUES ('Харрисон Форд отсудил компенсацию за сломанную на съемках «Звездных войн» ногу','Американский актер Харрисон Форд отсудил у принадлежащей Walt Disney продюсерской компании Foodles Production 1,96 миллиона долларов компенсации за сломанную во время съемок фильма ногу. Как пишет «Би-би-си», суд британского города выписал компании штраф за нарушение техники безопасности.'),
			('Гидрометцентр: зима в Москве будет холоднее предыдущей','Директор Гидрометцентра РФ Роман Вильфанд рассказал о составленном на предстоящую зиму вероятностном прогнозе погоды в Москве. Согласно нему, суровой зимы москвичам ждать не стоит — температура будет держаться около нормативных климатических значений или чуть выше.'),
			('США уничтожили в Йемене три радара после обстрела своего эсминца','Вооруженные силы США нанесли удары крылатыми ракетами по трем радиолокационным станциям (РЛС) в Йемене, уничтожив их, передает Reuters. Таким образом Вашингтон ответил на повторный обстрел своего эсминца «Мэйсон» у берегов этой страны, сообщили в Пентагоне. Напомним, американский военный корабль попадал под обстрел 10 и 13 октября. Огонь велся с территории, подконтрольной хуситам.'),
			('Керри настаивает на немедленном прекращении боевых действий в Йемене','"У США вызывает глубокую обеспокоенность авиаудар коалиции во главе с Саудовской Аравией по траурной процессии в Йемене, в результате которого погибло более 200 человек. Поэтому мы приветствуем намерение принца начать расследование этого инцидента. Призваем принять шаги, чтобы подобные инциденты не повторялись", - сказал Керри.'),
			('На дочь обидевшего Кадырова чемпиона мира совершено нападение в Москве','На дочь знаменитого бойца, неоднократного чемпиона мира в тяжелом весе, было совершено нападение в Москве. Неизвестный (или неизвестные) ударили девятилетнюю девочку в грудь, в итоге она попала в больницу.');
			
			
			
===========================================================		




ALTER TABLE users 
	ADD password VARCHAR(50) DEFAULT NULL,
	ADD admin CHAR(4) DEFAULT NULL;



INSERT INTO users (email, login, name, age, password, admin) 
	VALUES ('jane@mail.com', 'admin', 'Jane', '32', '123', 'true');



UPDATE users
	SET password=123;


ALTER TABLE users 
	MODIFY password VARCHAR(50) NOT NULL;





	
	
=========================================================



/* AUTHORS */


CREATE TABLE authors (
	id MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	firstname VARCHAR(20),
	lastname VARCHAR(20)
)ENGINE=innoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


INSERT INTO authors (firstname, lastname)
	VALUES('Александр', 'Дюма'), ('Максим', 'Горький'), ('Варлам', 'Шарламов');
	

ALTER TABLE articles 
	ADD COLUMN author_id MEDIUMINT(8) UNSIGNED NOT NULL;
	


ALTER TABLE articles
	ADD CONSTRAINT
	FOREIGN KEY(author_id) 
	REFERENCES authors(id);



UPDATE articles
	SET
		author_id = 2
	WHERE id >= 7;
	
	
UPDATE authors
	SET
		firstname = 'Алексей',
		lastname = 'Костарев'
	WHERE id = 1;
	
	
UPDATE authors
	SET
		firstname = 'Викрам',
		lastname = 'Васвани'
	WHERE id = 2;

	

	
=========================================================















