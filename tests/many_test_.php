<?php
require_once __DIR__ . '/../autoload.php';

use App\models;

echo '<pre>';


$arrObj = ['id' => 3, 'name' => 'John', 'age' => 23, 'email' => '@mail.ru'];

$emptyArr = [];

$skip = ['id', 'email'];

foreach ($arrObj as $key => $val) {
    foreach ($skip as $skipVal) {
        if ($key == $skipVal) {
            continue 2;
        }
    }
    $emptyArr[$key] = $val;
}

print_r($emptyArr);





























/*



// foreach только публичные методы

$user = new Models\User();

$user->age = 22;

$user->save();

foreach($user as $key => $val){
    echo $key .' => '. $val . "\n";
}










$article = new Models\Article();

$article->id = 15;
$article->title = 'Test Save';
$article->content = 'Test Save';

$article->save();

//var_dump($article);

var_dump(Models\Article::findById($article->id));










$article->title = 'Test insert';
$article->content = 'Test insert';

$article->insert();

var_dump($article);










// EMPTY ARRAY == (bool)FALSE

$data = Models\User::findById(11);

echo '<pre>';
var_dump($data);
print_r($data);


$arr = [];

if ( $arr ) echo 'ARR';
 */